﻿//
// Dynamsoft JavaScript Library for Basic Initiation of Dynamic Web TWAIN
// More info on DWT: http://www.dynamsoft.com/Products/WebTWAIN_Overview.aspx
//
// Copyright 2015, Dynamsoft Corporation 
// Author: Dynamsoft Team
// Version: 11.2
//
/// <reference path="dynamsoft.webtwain.initiate.js" />
var Dynamsoft = Dynamsoft || { WebTwainEnv: {} };

Dynamsoft.WebTwainEnv.AutoLoad = true;
///
Dynamsoft.WebTwainEnv.Containers = [{ContainerId:'dwtcontrolContainer', Width:270, Height:350}];
///
Dynamsoft.WebTwainEnv.ProductKey = 'B8C236A22680A80A23F540E7B869B5624DD44BF7B9903275EA929B85F573125C26B52776ACE1BCF12BC4FFBA9F9120F01A9E6BB7355C5A3F166C24C0BEAB08AFAF62BBFE04BC86034CCDFB2E5BD448C3FFBD479A60E138B39B019E95E0FA2AB27648C8F81877CA3E3411FA63335BDFA268751272050616E5C3562BE287FB0012093459EC2864BDD6AD3BCE4E08A72C475BD17C1AF40D6280450F8B5DCD83639FCDD34D528BF41ED1E4AEBC0AC51A2C9B6706376A9E895260296A7752B4D8B115F046165E947CD976EC4F2FA2498660BB1CB6460ADA038F37A642AD89458A4B65863040B0A31B32B596D09694308F20B2F7AFDB7C5455BD395B42A6';
///
Dynamsoft.WebTwainEnv.Trial = true;
///
Dynamsoft.WebTwainEnv.ActiveXInstallWithCAB = false;
///
Dynamsoft.WebTwainEnv.Debug = false; // only for debugger output
///
// Dynamsoft.WebTwainEnv.ResourcesPath = 'Resources';

/// All callbacks are defined in the dynamsoft.webtwain.install.js file, you can customize them.

// Dynamsoft.WebTwainEnv.RegisterEvent('OnWebTwainReady', function(){
// 		// webtwain has been inited
// });

