# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: ankccom_simyanduu
# Generation Time: 2016-01-15 06:39:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table dp_m_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dp_m_menu`;

CREATE TABLE `dp_m_menu` (
  `id_menu` smallint(3) NOT NULL AUTO_INCREMENT,
  `menu` varchar(45) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `module_menu` varchar(45) DEFAULT NULL,
  `class_menu` varchar(45) DEFAULT NULL,
  `function_menu` varchar(45) DEFAULT NULL,
  `widget` enum('Y','N') NOT NULL DEFAULT 'N',
  `icon` varchar(45) DEFAULT NULL,
  `stat_aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `urut` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

LOCK TABLES `dp_m_menu` WRITE;
/*!40000 ALTER TABLE `dp_m_menu` DISABLE KEYS */;

INSERT INTO `dp_m_menu` (`id_menu`, `menu`, `parent`, `module_menu`, `class_menu`, `function_menu`, `widget`, `icon`, `stat_aktif`, `urut`)
VALUES
	(1,'Beranda',0,'home','home','home','N','ti-home','Y',1),
	(2,'Persuratan',0,'persuratan','','#','N','ti-email','Y',2),
	(3,'Inbox',2,'persuratan','inbox','inbox','N',NULL,'Y',1),
	(4,'Surat Masuk',2,'persuratan','surat-masuk','surat_masuk','N',NULL,'Y',2),
	(5,'Surat Keluar',2,'persuratan','surat-keluar','surat_keluar','N',NULL,'Y',3),
	(6,'Laporan Persuratan',2,'persuratan','laporan-persuratan','laporan_persuratan','N',NULL,'Y',4),
	(8,'SKPD',0,'skpd','','#','N','ti-layout-grid2','Y',3),
	(9,'Proses Rekomendasi Izin',8,'skpd','prosesrekom','skpd','N',NULL,'Y',1),
	(10,'Laporan',8,'skpd','laporan','laporan','N',NULL,'Y',2),
	(11,'Arsip Perizinan',0,'arsip',NULL,'#','N','ti-briefcase','Y',4),
	(12,'Perekaman Arsip',11,'arsip','entry','entry','N',NULL,'Y',1),
	(13,'Pencarian Arsip',11,'arsip','pencarian','pencarian','N',NULL,'Y',2),
	(14,'Bank Data Perizinan',0,'bankdata',NULL,'#','N','ti-server','Y',5),
	(15,'Data Perizinan',14,'bankdata','perizinan','perizinan','N',NULL,'Y',1),
	(16,'Data Pemohon',14,'bankdata','pemohon','pemohon','N',NULL,'Y',2),
	(17,'Master Data',0,'masterdata',NULL,'#','N','ti-key','Y',6),
	(18,'Master Data Jenis Surat',17,'masterdata','master-data-jenis-surat','mst_jenis_surat','N',NULL,'Y',1),
	(19,'Master Data Surat Keluar',17,'masterdata','master-data-surat-keluar','mst_surat_keluar','N',NULL,'Y',2),
	(20,'Master Data Sifat Disposisi',17,'masterdata','master-data-sifat-disposisi','mst_sifat_disposisi','N',NULL,'Y',3),
	(21,'Master Data Pejabat',17,'masterdata','master-data-pejabat','mst_pejabat','N',NULL,'Y',4),
	(22,'Master Data SKPD',17,'masterdata','master-data-skpd','mst_skpd','N',NULL,'Y',5),
	(23,'Setting',0,'config',NULL,'#','N','ti-settings','Y',7),
	(24,'User Management',23,'config','user-management','user','N',NULL,'Y',1),
	(25,'Master Role',23,'config','role','role','N',NULL,'Y',2),
	(26,'Master Menu',23,'config','menu','menu','N',NULL,'Y',3),
	(27,'Role Menu',23,'config','role_menu','role_menu','N',NULL,'Y',4);

/*!40000 ALTER TABLE `dp_m_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dp_m_menu_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dp_m_menu_role`;

CREATE TABLE `dp_m_menu_role` (
  `id_menu_role` smallint(3) NOT NULL AUTO_INCREMENT,
  `id_role` smallint(3) NOT NULL,
  `id_menu` smallint(3) NOT NULL,
  `priority` smallint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_menu_role`),
  KEY `fk_m_menu_role_m_role1_idx` (`id_role`),
  KEY `fk_m_menu_role_m_menu1_idx` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

LOCK TABLES `dp_m_menu_role` WRITE;
/*!40000 ALTER TABLE `dp_m_menu_role` DISABLE KEYS */;

INSERT INTO `dp_m_menu_role` (`id_menu_role`, `id_role`, `id_menu`, `priority`)
VALUES
	(2,1,1,0),
	(3,1,2,0),
	(4,1,3,0),
	(5,1,4,0),
	(6,1,5,0),
	(7,1,6,0),
	(8,1,8,0),
	(9,1,9,0),
	(10,1,10,0),
	(11,1,11,0),
	(12,1,12,0),
	(13,1,13,0),
	(14,1,14,0),
	(15,1,15,0),
	(16,1,16,0),
	(17,1,17,0),
	(18,1,18,0),
	(19,1,19,0),
	(20,1,20,0),
	(21,1,21,0),
	(22,1,22,0),
	(23,1,23,0),
	(24,1,24,0),
	(25,1,25,0),
	(26,1,26,0),
	(27,1,27,0),
	(28,2,17,0),
	(29,2,18,0),
	(30,2,19,0),
	(31,2,20,0),
	(32,2,21,0),
	(33,2,22,0),
	(34,2,23,0),
	(35,2,24,0),
	(36,2,25,0),
	(37,2,26,0),
	(38,2,27,0);

/*!40000 ALTER TABLE `dp_m_menu_role` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
